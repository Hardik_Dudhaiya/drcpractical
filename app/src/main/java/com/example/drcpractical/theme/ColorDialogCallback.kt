package com.example.drcpractical.theme

interface ColorDialogCallback {
    fun onChosen(chosenThemeId : Int)
    fun onSave()
}